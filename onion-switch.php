<?php
/*
Plugin Name: Onion Switch
*/

class Onion_Switch {

	public function __construct() {

		add_filter( 'the_content', array( &$this, 'do_switch' ) );

		add_shortcode( 'onion', array( &$this, 'do_shortcode' ) );

	}

	public function is_onion() {

		$url = get_site_url();

		if( preg_match( '/https?:\/\/[^\/]*\.onion/', $url ) ) {
			return true;
		} else {
			return false;
		}

	}

	public function do_switch( $content = '' ) {

		if( !$this->is_onion() ) {
			return $content;
		}

		$link_pattern = '/(<a[^>]+?)href\s*=\s*"([^"]*)"\s+onion\s*=\s*"([^"]*)">/';

		$content = preg_replace( $link_pattern, '\1 href="\3">', $content );

		return $content;
	}

	public function do_shortcode( $atts, $content = '' ) {

		$atts = shortcode_atts(
			array(
				'href' => '',
				'onion' => '',
			),
			$atts
		);

		if( $this->is_onion() ) {
			$link = "<a href=\"{$atts['onion']}\">{$content}</a>";
		} else {
			$link = "<a href=\"{$atts['href']}\">{$content}</a>";
		}

		return do_shortcode( $link );
	}

}

$onion_switch = new Onion_Switch();

/* EOF */
