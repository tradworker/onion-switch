# Onion Switch

When a website exists within an ecosystem of websites which exist on
both the Corporate Internet and the Dark Web, one needs to link to the
correct address within the correct context. 
## Usage

With _Onion Switch_, one
can simply add `onion="http://facebookcorewwwi.onion.com/"` after the
`href=""` attribute in a hyperlink tag. The plugin will check to
see if you're on the Dark Web and replace the `href` with the `onion`
address if you are. When on the Corporate Internet, it links to the
corporate URL. When on the Dark Web, it links to the Dark Web URL.

### Prerequisites

WordPress

### Installing

Standard WordPress plugin

## Deployment

Standard WordPress plugin

## Authors

* **Matt Parrott** - *Author* - [wikitopian](https://www.gitlab.com/wikitopian)

## License

GPLv2

## Acknowledgments

* [The TOR Project](https://www.torproject.org)
* [WordPress](https://www.wordpress.org)
